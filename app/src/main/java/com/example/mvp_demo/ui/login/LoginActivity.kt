package com.example.mvp_demo.ui.login

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.mvp_demo.R
import com.example.mvp_demo.ui.presenter.presenter
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {

    var presenter1: presenter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        presenter1 = presenter(this)
        btnClick()
    }

    private fun btnClick() {

        btn_login.setOnClickListener {
            presenter1?.checkLogin(edt_username.text, edt_password.text)
        }
    }
}