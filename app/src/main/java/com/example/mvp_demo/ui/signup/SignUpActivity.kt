package com.example.mvp_demo.ui.signup

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.mvp_demo.R
import com.example.mvp_demo.ui.presenter.presenter
import kotlinx.android.synthetic.main.activity_sign_up.*

class SignUpActivity : AppCompatActivity() {

    val presenter: presenter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)

        presenter(this)
        btnClick()
    }

    private fun btnClick() {

        btn_signup.setOnClickListener {
            presenter!!.checkSignUp(edt_name.text,
                edt_email.text,
                edt_pass.text,
                edt_cnfrm_pass.text)
        }
    }
}