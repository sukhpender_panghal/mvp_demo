package com.example.mvp_demo.ui.presenter

import android.content.Context
import android.text.Editable
import android.text.TextUtils
import android.widget.Toast
import com.example.mvp_demo.model.LoginResponse
import com.example.mvp_demo.network.ApiResponse
import com.example.mvp_demo.network.ApiRetrofit
import com.example.mvp_demo.network.ApiService
import com.google.gson.JsonObject
import retrofit2.Response

class presenter(var context: Context) : ApiResponse {

    val retro1 = ApiRetrofit().create()

    fun checkLogin(
        username: Editable,
        password: Editable
    ) {
        if (TextUtils.isEmpty(username)
            && TextUtils.isEmpty(password)
        ) {
            Toast.makeText(context, "please fill the details", Toast.LENGTH_SHORT).show()
        } else {
            callLoginApi(username.toString(), password.toString())
        }
    }

    private fun callLoginApi(
        username: String,
        password: String
    ) {
        val jsonObject = JsonObject()
        jsonObject.addProperty("username", username)
        jsonObject.addProperty("password", password)

        val apiService = ApiService<LoginResponse>()
        apiService.hit(this, retro1.login(jsonObject))
    }

    fun checkSignUp(
        name: Editable,
        email: Editable,
        pass: Editable,
        cnfrmPass: Editable
    ) {
        if (TextUtils.isEmpty(name) &&
            TextUtils.isEmpty(email) &&
            TextUtils.isEmpty(pass) &&
            TextUtils.isEmpty(cnfrmPass)
        ) {
            Toast.makeText(context, "please fill the details", Toast.LENGTH_SHORT).show()
        } else {
            callSignUp(name.toString(), email.toString(), pass.toString(), cnfrmPass.toString())
        }
    }

    private fun callSignUp(
        name: String,
        email: String,
        password: String,
        confirm_password: String
    ) {
        val jsnObjct = JsonObject()
        jsnObjct.addProperty("name", name)
        jsnObjct.addProperty("email", email)
        jsnObjct.addProperty("password", password)
        jsnObjct.addProperty("confirm_password", confirm_password)

        val apiService = ApiService<SignUpResponse>()
        apiService.hit(this,retro1.signUp(jsnObjct))
    }

    fun responseSuccess(response: Response<*>?): Response<*>? {
        if (response!!.isSuccessful == true && response.body() != null){
        }else
            false
        return response
    }
    override fun onResponse(response: Response<Any>) {
        if (responseSuccess(response) != null){

        }

    }
    override fun onError(t: Throwable) {
    }
}