package com.example.mvp_demo.network

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ApiService<T> {

    fun hit( apiResponse: ApiResponse,call1: Call<Any>) {
        call1.enqueue(object : Callback<Any> {
            override fun onFailure(call: Call<Any>?, t: Throwable) {
                apiResponse.onError(t)
            }

            override fun onResponse(call: Call<Any>?, response: Response<Any>) {
                apiResponse.onResponse(response)
            }
        })
    }
}