package com.example.mvp_demo.network

import retrofit2.Response

interface ApiResponse {
    fun onResponse(response: Response<Any>)
    fun onError(t: Throwable)
}