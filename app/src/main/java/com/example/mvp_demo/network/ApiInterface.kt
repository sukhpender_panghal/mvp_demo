package com.example.mvp_demo.network

import com.example.mvp_demo.model.LoginResponse
import retrofit2.http.Query

interface ApiInterface {
    fun login(
        @Query("username") username: String,
        @Query("password") password: String
    ): retrofit2.Call<LoginResponse>

    fun signUp()

}